# Python on the Server

A portable installation of Python using the Micromamba virtual environment and package manager.

## Directory Structure

    /                           # Base Directory
    ├── apps                    # Application Directory
    │   ├── .conda              # Anaconda Config Files
    │   ├── .micromamba         # Micromamba Config Files
    │   ├── bin                 # Binaries
    │   │   ├── micromamba.bat  # CMD Micromamba Initialization
    │   │   └── micromamba.exe  # Micromamba (1, 2)
    │   └── micromamba.lnk      # Shortcut to use micromamba in CMD (3, 4)
    └── ...                     # Other files at the server

## Instructions

1. [Download Micromamba](https://micro.mamba.pm/api/micromamba/win-64/latest) (`micromamba.tar.bz2`)
2. Extract `micromamba.exe` (using Z-Zip or similar) to the `bin` directory
3. Create a shortcut for the command: `%COMSPEC% /k .\bin\micromamba.bat`
4. Use Micromamba in CMD by clicking the shortcut

Based on the [Official Installation Instructions](https://mamba.readthedocs.io/en/latest/installation.html#windows).

## Examples

```bat
:: Access Micromamba with either of the following alias
micromamba
conda
mamba
mm
```

```bat
:: Create a new Virtual Environment name test
mm create -n test

:: Activate the Virtual Environment named test
mm activate test

:: Install a package (pandas) in the active Virtual Environment
mm install pandas

:: Run Python in the active Virtual Environment
python
```

```bat
:: Create a new Virtual Environment name test and install the Pandas package
mm create -n test pandas
```

```bat
:: Create a new Virtual Environment from an environment specification file
micromamba create -f env.yml
```

Read more in the [Official Documentation](https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html).
