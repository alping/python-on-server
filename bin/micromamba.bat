@echo Micromamba Environment

@echo off
title Micromamba

:: Environmental Variables
set USERPROFILE=/apps
set PATH=%PATH%;%USERPROFILE%/bin
set MAMBA_ROOT_PREFIX=%USERPROFILE%/.micromamba

:: Alias
DOSKEY conda=micromamba $*
DOSKEY mamba=micromamba $*
DOSKEY mm=micromamba $*

:: Initialize Micromamba Shell if necessary
if not exist %MAMBA_ROOT_PREFIX% (
    micromamba shell hook -s cmd.exe
)

:: Avoid possible conflict with other installed conda/mamba
set CONDA_SHLVL=
@CALL %MAMBA_ROOT_PREFIX%/condabin/mamba_hook.bat

cd /
@echo on
